import json

from typing import (
    Dict,
    List,
    Optional,
    Tuple,
)

import poetry_version

__version__ = poetry_version.extract(source_file=__file__)

def run_turing_machine(
    machine_config: Dict,
    input_string: str,
    max_steps: Optional[int] = None,        
) -> Tuple[str, List, bool]: 

    # Taille du tampon pour le ruban de la machine de Turing
    buffer_size = 10 
    # Initialisation du ruban avec des blancs avant et après l'entrée
    tape = machine_config.get("blank") * buffer_size + input_string + machine_config.get("blank") * buffer_size

    # État initial de la machine
    current_state = machine_config.get("start state")
    # Premier symbole à lire sur le ruban
    current_symbol = tape[buffer_size]
    # Position initiale de la tête de lecture
    head_position = 0
    # Mémoire initiale du ruban
    tape_memory = " " * buffer_size + input_string + " " * buffer_size
    # Historique des étapes d'exécution
    execution_trace = []

    # Boucle jusqu'à atteindre un état final
    while current_state not in machine_config["final states"]:
        
        # Enregistrement de l'état actuel, du symbole lu, de la position et de la transition
        trace_entry = {
            "state": current_state,
            "reading": current_symbol,
            "position": head_position,
            "memory": tape_memory.strip(),
            "transition": machine_config['table'][current_state][current_symbol]
        }

        # Ajout de l'entrée de trace à l'historique
        execution_trace.append(trace_entry)

        # Instructions de transition basées sur l'état actuel et le symbole lu
        transition_instructions = machine_config['table'][current_state][current_symbol]

        # Si les instructions sont un dictionnaire (plusieurs actions à exécuter)
        if isinstance(transition_instructions, dict):
            for instruction_key in transition_instructions:
            
                if instruction_key == 'write':
                    # Écriture sur le ruban et mise à jour de la mémoire
                    tape = list(tape)
                    tape[head_position + buffer_size] = transition_instructions[instruction_key]
                    tape = "".join(tape)

                    tape_memory = list(tape_memory)
                    tape_memory[head_position + buffer_size] = transition_instructions[instruction_key]
                    tape_memory = "".join(tape_memory)
                    
                if instruction_key == 'L':
                    # Déplacement de la tête à gauche et changement d'état
                    head_position -= 1
                    current_state = transition_instructions[instruction_key]

                if instruction_key == 'R':
                    # Déplacement de la tête à droite et changement d'état
                    head_position += 1
                    current_state = transition_instructions[instruction_key]
                    
        
        if isinstance(transition_instructions, str):
            if transition_instructions == 'L':
                head_position -= 1

            if transition_instructions == 'R':
                head_position += 1
        
        
        current_symbol = tape[head_position + buffer_size]

    
    return tape_memory.strip(), execution_trace, True
